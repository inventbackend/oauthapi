package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.mdlClientID;
import model.mdlCustomer;
import model.mdlQueryExecute;

public class CustomerAdapter {

	public static model.mdlCustomer GetCustomerData(String CustomerID){
		model.mdlCustomer mdlCustomer = new model.mdlCustomer();
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		
		try{
			sql = "{call sp_GetCustomerData(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", CustomerID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_GetCustomerData");
			while(jrs.next()){
				mdlCustomer.CustomerID = jrs.getString("CustomerID");
				mdlCustomer.CustomerName = jrs.getString("CustomerName");
				mdlCustomer.CustomerAddress = jrs.getString("CustomerAddress");
				mdlCustomer.Phone = jrs.getString("Phone");
				mdlCustomer.PIC = jrs.getString("PIC");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "getCustomerData", sql , CustomerID);
		}
		
		return mdlCustomer;
	}

	public static String InsertCustomerData(mdlCustomer param)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_InsertCustomer(?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CustomerName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CustomerAddress));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.Phone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.PIC));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "InsertCustomerData");

			result = success == true ? "Success Insert Customer" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertCustomerData", sql , param.CustomerID);
			result = "Database Error";
		}

		return result;
	}
	
	public static String UpdateCustomerData(mdlCustomer param)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateCustomer(?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CustomerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CustomerName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.CustomerAddress));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.Phone));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.PIC));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "UpdateCustomerData");

			result = success == true ? "Success Update Customer" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateCustomerData", sql , param.CustomerID);
			result = "Database Error";
		}

		return result;
	}
	
	public static String DeleteCustomerData(String CustomerID)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_DeleteCustomer(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", CustomerID));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "DeleteCustomerData");

			result = success == true ? "Success Delete Customer" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteCustomerData", sql , CustomerID);
			result = "Database Error";
		}

		return result;
	}
}
