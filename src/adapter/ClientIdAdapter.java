package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import adapter.Base64Adapter;
import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlClientID;
import model.mdlQueryExecute;

public class ClientIdAdapter {
	public static String getAuthorizationKey(String ClientID, String ClientSecret){
		String stringToSign = ClientID + ":" + ClientSecret;
		String authorizationKey  = Base64Adapter.EncriptBase64(stringToSign); //generate apiKey
		return authorizationKey;
	}
	
	public static String getClientID(String UserName, String AppName){
		String ClientID = "";
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		
		try{
			sql = "{call sp_GetClientID(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", UserName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", AppName));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_GetClientID");
			while(jrs.next()){
				String originalClientID = jrs.getString("UserName")+"&&"+jrs.getString("AppName");
				ClientID = EncryptAdapter.encrypt(originalClientID, Globals.keyAPI);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "getClientID", sql , UserName);
		}
		
		return ClientID;
	}
	
	public static Boolean checkClientID(String UserName, String AppName){
		Boolean exist = false;
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		
		try{
			sql = "{call sp_GetClientID(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", UserName));
			listParam.add(QueryExecuteAdapter.QueryParam("string", AppName));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_GetClientID");
			while(jrs.next()){
				exist = true;
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "checkClientID", sql , UserName);
		}
		
		return exist;
	}
}
