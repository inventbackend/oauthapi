package adapter;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.log4j.Logger;

import controller.controller;
import com.google.gson.Gson;

import model.Globals;
import model.mdlAuthentication;
/** Documentation
 *
 */
public class TokenAdapter {
	final static Logger logger = Logger.getLogger(controller.class);
	public static model.mdlToken GetToken()
	{
		model.mdlToken mdlToken = new model.mdlToken();

		if (Globals.token == null || Globals.token.equalsIgnoreCase("")){
			try {
				mdlToken = GetNewToken();
			}
			catch (Exception ex) {
				Globals.token = "";
			  }
		}else{
			try{
				Date dateNow = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				Date dateToken = df.parse(Globals.tokenDate.replace("T", " "));
				long toMinute = 1000 * 60;
				int dateDiff = (int) (dateNow.getTime() - dateToken.getTime());
				int diffMinute = (int) (dateDiff / toMinute);

				if (diffMinute >= 55){
					mdlToken = GetNewToken();
				}else{
					mdlToken.access_token = Globals.token;
				}
			}catch(Exception ex){

			}
		}
		return mdlToken;
	}

	public static model.mdlToken GetNewToken() throws KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, GeneralSecurityException, IOException, NamingException{
		model.mdlToken mdlToken = new model.mdlToken();
		String jsonOut = "";

		// Get the base naming context from web.xml
		 Context context = (Context)new InitialContext().lookup("java:comp/env");

		// Get a single value from web.xml
		 String APIGatewayIPAddress = (String)context.lookup("param_ip_token");
		 String ClientID = (String)context.lookup("param_clientid");
		 String ClientSecret = (String)context.lookup("param_clientsecret");

		 //decrypt key
		 String decryptedClientID = EncryptAdapter.decrypt(ClientID, Globals.keyAPI) ;
		 String decryptedClientSecret = EncryptAdapter.decrypt(ClientSecret, Globals.keyAPI) ;
		 HostnameVerifier hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
		 ClientConfig config = new DefaultClientConfig();
		 SSLContext ctx = SSLContext.getInstance("SSL");
		 TrustManager[] trustAllCerts = { new InsecureTrustManagerAdapter() };
		 ctx.init(null, trustAllCerts, null);
		 config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hostnameVerifier, ctx));
		 Client client = Client.create(config);

		//Client client = Client.create();
		String urlAPI = APIGatewayIPAddress + "/api/oauth/token";
		String authorizationKey = ClientIdAdapter.getAuthorizationKey(decryptedClientID, decryptedClientSecret);
		WebResource webResource = client.resource(urlAPI);

		//add form variables
       Form form = new Form();
       form.add("grant_type", "client_credentials");
		try {
            ClientResponse response  = webResource.type("application/x-www-form-urlencoded")
					  .header("Authorization","Basic "+ authorizationKey)
					  .post(ClientResponse.class,form);

			jsonOut = response.getEntity(String.class);
			Gson gson = new Gson();
			mdlToken = gson.fromJson(jsonOut, model.mdlToken.class);
			logger.info("SUCCESS GetNewToken = URL :" + urlAPI +", authorizationKey:" + authorizationKey
					 + ", ClientID:"+ decryptedClientID +", ClientSecret:" + decryptedClientSecret + ", jsonOut:" + jsonOut);
		}
		catch (Exception ex) {
			mdlToken.access_token = "";
			logger.error("FAILED = URL :" + urlAPI +", authorizationKey:" + authorizationKey
					+ ", ClientID:"+ decryptedClientID +", ClientSecret:" + decryptedClientSecret
					+ ", Exception:" + ex.toString(), ex);
		}
		Globals.token = mdlToken.access_token;
		Globals.tokenDate = LocalDateTime.now().toString();
		return mdlToken;
	}

	public static model.mdlAuthentication GetAuthentication(String token){
		model.mdlAuthentication mdlAuthentication = new model.mdlAuthentication();
		
		try{
			String decryptedToken = EncryptAdapter.decrypt(token, Globals.keyAPI);
			String[] decryptedTokenArray = decryptedToken.split("&&");
			String clientID = decryptedTokenArray[0];
			String tokenStartDate = decryptedTokenArray[1];
			String tokenExpiresIn = decryptedTokenArray[2];
			
			String decryptedClientID = EncryptAdapter.decrypt(clientID, Globals.keyAPI);
			String[] decryptedClientIDArray = decryptedClientID.split("&&");
			String userName = decryptedClientIDArray[0];
			String appName = decryptedClientIDArray[1];
			
			mdlAuthentication.UserName = userName;
			mdlAuthentication.AppName = appName;
			mdlAuthentication.TokenStartDate = tokenStartDate;
			mdlAuthentication.TokenExpiresIn = tokenExpiresIn;
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetAuthentication", "GetAuthentication" , "token: "+token);
		}
		
		return mdlAuthentication;
	}

	public static Boolean CheckToken(String tokenStartDate, String tokenExpiresIn, String token){
		Boolean isActive = false;
		
		try{
			Date now = new Date();
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			Date parsedTokenStartDate = df.parse(tokenStartDate);
			long seconds = (now.getTime()-parsedTokenStartDate.getTime())/1000;
			
			if(seconds <= Long.parseLong(tokenExpiresIn))
				isActive = true;
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckToken", "CheckToken" , "token: "+token);
		}
		
		return isActive;
	}
}
