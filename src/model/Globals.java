package model;

import javax.naming.Context;
import javax.naming.InitialContext;

public class Globals {
	public static String gReturn_Status = "";
	public static String gCommand = "";
	public static String gCondition = "";
	public static String gConditionDesc = "";
    public static String token = "";
    public static String tokenDate = "";
    public static String keyAPI = "inventlogistic";
}
