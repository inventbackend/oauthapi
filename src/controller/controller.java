package controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import controller.controller;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

import adapter.ClientIdAdapter;
import adapter.ConvertDateTimeHelper;
import adapter.CustomerAdapter;
import adapter.EncryptAdapter;
import adapter.HeaderAdapter;
import adapter.InsecureTrustManagerAdapter;
//import adapter.LogAdapter;
import adapter.TokenAdapter;
import model.Globals;
import model.mdlClientID;
import model.mdlResult;

import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.servlet.http.HttpServletResponse;

@RestController
public class controller {
	final static Logger logger = Logger.getLogger(controller.class);

	@RequestMapping(value = "/ping",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody void GetPing()
	{
		return;
	}

	@RequestMapping(value = "/callAPI",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody String CallAPI(@RequestBody model.mdlAPI mdlAPI) throws NamingException, KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, GeneralSecurityException, IOException
	{
		String jsonResult = "";
		Globals.keyAPI = mdlAPI.keyAPI;
		Gson gson = new Gson();
		// Get the base naming context from web.xml
		Context context = (Context)new InitialContext().lookup("java:comp/env");

		// Get a single value from web.xml
		String APIGatewayIPAddress = (String)context.lookup("param_ip_api_gw");
		String XBCAClientID = (String)context.lookup("param_xbca_clientid");
		String APISecret = (String)context.lookup("param_apisecret");
		String APIKey = (String)context.lookup("param_apikey");

		String decryptedAPISecret = EncryptAdapter.decrypt(APISecret, Globals.keyAPI);
		String decryptedAPIKey = EncryptAdapter.decrypt(APIKey, Globals.keyAPI);
		String decryptedXBCAClientID = EncryptAdapter.decrypt(XBCAClientID, Globals.keyAPI);

		model.mdlToken mdlToken = TokenAdapter.GetToken();
		String urlAPI = mdlAPI.urlAPI;
		String urlFinal = APIGatewayIPAddress + urlAPI;
		String methodAPI = mdlAPI.methodAPI;
		String jsonIn = mdlAPI.contentAPI == null ? "" : mdlAPI.contentAPI;

		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		
		HostnameVerifier hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
		ClientConfig config = new DefaultClientConfig();
		SSLContext ctx = SSLContext.getInstance("SSL");
		TrustManager[] trustAllCerts = { new InsecureTrustManagerAdapter() };
		ctx.init(null, trustAllCerts, null);
		config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hostnameVerifier, ctx));
		
		Client client = Client.create(config);

//		Client client = Client.create();
		WebResource webResource = client.resource(urlFinal);
		ClientResponse response = null;

		//canonicalize JSON (remove all whitespace like \r, \n, \t and space)
		String bodyToHash = jsonIn.replaceAll("\\s+", "");
		model.mdlHeader mdlHeader = HeaderAdapter.GetHeaders(methodAPI, urlAPI, mdlToken.access_token, bodyToHash, decryptedAPIKey, decryptedAPISecret);

		try{
		    Builder builder = webResource.type("application/json")
				 .header("Authorization", "Bearer " + mdlToken.access_token)
				 .header("X-BCA-Key", mdlHeader.Key)
				 .header("X-BCA-Timestamp", mdlHeader.Timestamp)
				 .header("X-BCA-Signature", mdlHeader.Signature)
				 .header("X-BCA-ClientID", decryptedXBCAClientID);
		    
			 if (methodAPI.equalsIgnoreCase("POST")){
				 response = builder.post(ClientResponse.class,jsonIn);
			 }else if (methodAPI.equalsIgnoreCase("GET")){
				 response = builder.get(ClientResponse.class);
			 }else if (methodAPI.equalsIgnoreCase("PUT")){
				 response = builder.put(ClientResponse.class,jsonIn);
			 }else if (methodAPI.equalsIgnoreCase("DELETE")){
				 response = builder.delete(ClientResponse.class, jsonIn);
			 }

			 jsonResult = response.getEntity(String.class);
			 //check if authorized or not.. if not authorized, get new token and then call API again
			 String responseStatus = Integer.toString(response.getStatus());

			 if (responseStatus.equalsIgnoreCase("401") ){
				 try{
					 mdlErrorSchema = gson.fromJson(jsonResult, model.mdlErrorSchema.class);
				 }catch(Exception e){
					 logger.error("FAILED = URL :"+urlFinal+", method: "+methodAPI+", jsonIn:" + jsonIn + ", X-BCA-Key:" + mdlHeader.Key
							 + ", X-BCA-Timestamp:"+ mdlHeader.Timestamp + ", X-BCA-Signature:" + mdlHeader.Signature
							 + ", X-BCA-ClientID:" + decryptedXBCAClientID + ", jsonOut:" + jsonResult + "Exception:" + e.toString(), e);
					 jsonResult = "{\"Error\":\"" + e.toString() + "\"}";
				}

				if (mdlErrorSchema.ErrorCode != null && mdlErrorSchema.ErrorCode.equalsIgnoreCase("ESB-14-009") &&
					mdlErrorSchema.ErrorMessage.English.equalsIgnoreCase("Unauthorized")){
					mdlToken = TokenAdapter.GetNewToken();
					mdlHeader = HeaderAdapter.GetHeaders(methodAPI, urlAPI, mdlToken.access_token, bodyToHash, decryptedAPIKey, decryptedAPISecret);
					
					builder = webResource.type("application/json")
						.header("Authorization", "Bearer " + mdlToken.access_token)
						.header("X-BCA-Key", mdlHeader.Key)
						.header("X-BCA-Timestamp", mdlHeader.Timestamp)
						.header("X-BCA-Signature", mdlHeader.Signature)
						.header("X-BCA-ClientID", decryptedXBCAClientID);
					
					if (methodAPI.equalsIgnoreCase("POST")){
						response = builder.post(ClientResponse.class, jsonIn);
					}else if (methodAPI.equalsIgnoreCase("GET")){
						response = builder.get(ClientResponse.class);
					}else if (methodAPI.equalsIgnoreCase("PUT")){
						response = builder.put(ClientResponse.class, jsonIn);
					}else if (methodAPI.equalsIgnoreCase("DELETE")){
						response = builder.delete(ClientResponse.class, jsonIn);
					}
					jsonResult = response.getEntity(String.class);
				}
			 }

			 
			 logger.info("SUCCESS callAPI = URL :"+urlFinal+", method: "+methodAPI + ", X-BCA-Key:" + mdlHeader.Key
					 + ", X-BCA-Timestamp:"+ mdlHeader.Timestamp + ", X-BCA-Signature:" + mdlHeader.Signature
					 + ", X-BCA-ClientID:" + decryptedXBCAClientID + ", jsonIn:" + jsonIn + ", jsonOut:" + jsonResult);
		}
		catch(Exception ex){
			logger.error("FAILED = URL :"+urlFinal+", method: "+methodAPI+", jsonIn:" + jsonIn + ", X-BCA-Key:" + mdlHeader.Key
					+ ", X-BCA-Timestamp:"+ mdlHeader.Timestamp + ", X-BCA-Signature:" + mdlHeader.Signature
					+ ", X-BCA-ClientID:" + decryptedXBCAClientID + ", jsonOut:" + jsonResult + "Exception:" + ex.toString(), ex);
			jsonResult = "{\"Error\":\"" + ex.toString() + "\"}";
		}

//	if (mdlAPI.urlAPI.contains("/passbooks/small-passbooks/account-statements"))
//	    jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"Transaction\":\"050417 KAS            4,000.00 K     610,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            5,000.00 K     615,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            6,000.00 K     621,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            7,000.00 K     628,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            8,000.00 K     636,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           10,000.00 K    646,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           11,000.00 K    657,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           12,000.00 K    669,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           13,000.00 K    682,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"}]}";
//			jsonResult = "{\"TransactionIndicator\":\"MC\",\"TransactionRecord\":[{\"Transaction\":\"100616 KAS            1,000.00 K       67,116,159.05 0900\",\"Trailer\":\"------ SETORAN MMMMMMMMMM 111111111111111 222222222222222\"}]}";
//	else if (mdlAPI.urlAPI.contains("/passbooks/regular-passbooks/account-statements"))
//	    jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"740,000,000.00\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"732,827,048.08\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"725,654,096.16\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"718,481,144.24\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"711,308,192.32\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"704,135,240.40\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"7,172,951.92\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"696,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"686,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"676,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"666,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"656,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"646,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"636,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"626,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"616,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"606,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"596,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"586,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"576,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"566,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"556,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"546,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"536,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"526,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"516,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"11/05/17\",\"TransactionAmount\":\"10,000,000.00\",\"TransactionCode\":\"0800\",\"RunningBalance\":\"506,962,288.48\",\"TransactionType\":\"Debit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"}]}";
//	else if (mdlAPI.urlAPI.contentEquals("/login/login-by-card"))
//		jsonResult = "[{\"AccountNumber\":\"06040010261\",\"AccountType\":\"108\",\"AccountTypeDescription\":\"Tahapan Xpresi\",\"AccountOwnership\": null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000003600\",\"CustomerName\":\"IUIOUIO\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]}]";
//		jsonResult = "[{\"AccountNumber\": \"00080002891\",\"AccountType\": \"110\",\"AccountTypeDescription\": \"Tahapan\",\"AccountOwnership\": null,\"CustomerDetails\": [{\"CustomerNumber\": \"00000003600\",\"CustomerName\": \"IUIOUIO\",\"RelationType\": \"5\",\"OwnerCode\": \"902\"},{\"CustomerNumber\": \"00000003551\",\"CustomerName\": \"LELIANI KUSUMA DEWI\",\"RelationType\": \"5\",\"OwnerCode\": \"901\"}]},{\"AccountNumber\": \"0100000181\",\"AccountType\": \"110\",\"AccountTypeDescription\": \"Tahapan Gold\",\"AccountOwnership\": null,\"CustomerDetails\": [{\"CustomerNumber\": \"00000003277\",\"CustomerName\": \"ANI\",\"RelationType\": \"0\",\"OwnerCode\": \"901\"}]},{\"AccountNumber\": \"00080004443\",\"AccountType\": \"107\",\"AccountTypeDescription\": \"TabunganKu\",\"AccountOwnership\": null,\"CustomerDetails\": [{\"CustomerNumber\": \"00000006909\",\"CustomerName\": \"MIRANDA\",\"RelationType\": \"0\",\"OwnerCode\": \"901\"}]}]";
//	else if (mdlAPI.urlAPI.contains("/alert/mm/status"))
//	    jsonResult = "{\"Status\":\"Taken\"}";
//	else if(mdlAPI.urlAPI.contains("/future-branch-customer-products/"))
//	    jsonResult = "{\"cin\": \"0000015420\",\"recommendation-product\": [{\"company-code\": \"00008\",\"company-name\": \"E-Channel\",\"product-code\": \"KBI08\",\"product-name\": \"Klik BCA\",\"product-priority\": \"1\",\"product-link\": {\"url-mybca\": \"http://mybcaportal/sites/applications/PenawaranSolusi/AllProduct/e-channel%20-%20KlikBCA.JPG\",\"url-bcacoid\": \"https://www.bca.co.id/id/Individu/Produk/E-Banking/Klik-BCA?type=mobile_apps\",\"url-futurebranch\": \"http:/10.20.214.203/futurebranch/sales/product/KlikBCA.jpg\",\"url-futurebranch-icon\": \"http:/10.20.214.203/futurebranch/sales/icon/KlikBCA.gif\",\"url-futurebranch-ads\": \"http:/10.20.214.203/futurebranch/sales/ads/KlikBCA.jpg\"},\"last-product\": {}},{\"company-code\": \"00008\",\"company-name\": \"E-Channel\",\"product-code\": \"KBI08\",\"product-name\": \"Klik BCA\",\"product-priority\": \"1\",\"product-link\": {\"url-mybca\": \"http://mybcaportal/sites/applications/PenawaranSolusi/AllProduct/e-channel%20-%20KlikBCA.JPG\",\"url-bcacoid\": \"https://www.bca.co.id/id/Individu/Produk/E-Banking/Klik-BCA?type=mobile_apps\",\"url-futurebranch\": \"http:/10.20.214.203/futurebranch/sales/product/KlikBCA.jpg\",\"url-futurebranch-icon\": \"http:/10.20.214.203/futurebranch/sales/icon/KlikBCA.gif\",\"url-futurebranch-ads\": \"http:/10.20.214.203/futurebranch/sales/ads/KlikBCA.jpg\"},\"last-product\": {}}],\"category-product\": [{\"company-code\": \"00001\",\"company-name\": \"AIA\"},{\"company-code\": \"00002\",\"company-name\": \"BCA LIFE\"},{\"company-code\": \"00003\",\"company-name\": \"BCA Insurance\"},{\"company-code\": \"00004\",\"company-name\": \"Kartu Kredit\"},{\"company-code\": \"00005\",\"company-name\": \"KPR\"},{\"company-code\": \"00006\",\"company-name\": \"Kredit Mobil\"},{\"company-code\": \"00007\",\"company-name\": \"Kredit Sepeda Motor\"},{\"company-code\": \"00008\",\"company-name\": \"E-Channel\"},{\"company-code\": \"00009\",\"company-name\": \"Kredit Modal Kerja\"},{\"company-code\": \"00010\",\"company-name\": \"Collection\"},{\"company-code\": \"00011\",\"company-name\": \"Investasi\"},{\"company-code\": \"00012\",\"company-name\": \"Simpanan\"}]}";
//		else if(mdlAPI.urlAPI.contentEquals("/alert/alert"))
//			jsonResult = "{\"Result\": \"ALT-002255WW-180912-00000007\"}";
//		if(mdlAPI.urlAPI.contentEquals("/device-management/login-device-management"))
//			jsonResult = "{\"Result\":\"true\"}";
//		else if(mdlAPI.urlAPI.contentEquals("/device-management/login-device-management-error"))
//			jsonResult = "{\"ErrorCode\":\"ESB-14-005\",\"ErrorMessage\": {\"Indonesian\":\"Sistem sedang dalam maintenance\",\"English\":\"System under maintenance\"}}";
//		else if(mdlAPI.urlAPI.contentEquals("/device-management/get-device-management?serial=D104182700226"))
//			jsonResult = "{\"SerialNumber\":\"D104182700226\",\"WSID\":\"00220001\",\"BranchCode\":\"0022\",\"BranchTypeID\":\"KCU\",\"BranchInitial\":\"MDN\",\"BranchName\":\"MEDAN\",\"BranchTypeName\":\"Kantor Cabang Utama\",\"UpdateDate\":\"2018-12-12 10:03:29\",\"UpdateBy\":\"futurebranch\"}";
//		else if(mdlAPI.urlAPI.contentEquals("/device-management/get-device-management-error?serial=D104182700226"))
//			jsonResult = "{\"ErrorCode\":\"01\",\"ErrorMessage\":{\"Indonesian\":\"Tidak Ada Device\",\"English\":\"No Device\"}}";
//			jsonResult = "";
//		else if(mdlAPI.urlAPI.contentEquals("/device-management/get-branch-list?serial=D104182700226&wsid=00220001"))
//			jsonResult = "[{\"BranchCode\":\"0001\",\"BranchName\":\"ASEMKA\",\"BranchTypeID\":\"KCU\",\"BranchTypeName\":\"Kantor Cabang Utama\",\"BranchInitial\":\"KPO\"}]";
//		else if(mdlAPI.urlAPI.contentEquals("/device-management/get-branch-list-error?serial=D104182700226&wsid=00220001"))
//			jsonResult = "{\"ErrorCode\":\"03\",\"ErrorMessage\":{\"Indonesian\":\"Cabang tidak ditemukan\",\"English\":\"Branch not found\"}}";
//		else if(mdlAPI.urlAPI.contentEquals("/device-management/update-device-management"))
//			jsonResult = "{\"Result\":\"true\"}";
//		else if(mdlAPI.urlAPI.contentEquals("/device-management/update-device-management-error"))
//			jsonResult = "{\"ErrorCode\":\"01\",\"ErrorMessage\":{\"Indonesian\":\"Kode Cabang Salah Input\",\"English\":\"Wrong Input Branch Code\"}}";
//		else if(mdlAPI.urlAPI.contentEquals("/login/login-by-card-error"))
//			jsonResult = "{\"ErrorCode\":\"03\",\"ErrorMessage\":{\"Indonesian\":\"Nomor kartu tidak ditemukan\",\"English\":\"Card number cannot be found\"}}";
//		else if(mdlAPI.urlAPI.contentEquals("/deposit-accounts/account-number-lists/00210001395%2C04010000021"))
//			jsonResult = "{\"AccountList\":[{\"AccountNumber\":\"00210001395\",\"AccountName\":\"\",\"AccountType\":\"\",\"AccountTypeDescription\":\"\",\"CustomerDetails\":[{\"CustomerNumber\":\"00000006508\",\"CustomerName\":\"JAYA KARINAA\",\"RelationType\":\"1\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"04010000021\",\"AccountName\":\"OMAR PUTIHRAI\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan\",\"CustomerDetails\":[{\"CustomerNumber\":\"00000000290\",\"CustomerName\":\"RICHARD MCAULIFFE\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]}]}";
//		else if(mdlAPI.urlAPI.contentEquals("/deposit-accounts/account-number-lists-error/00210001395%2C04010000021"))
//			jsonResult = "{\"ErrorCode\":\"ESB-99-161\",\"ErrorMessage\":{\"Indonesian\":\"Nomor Rekening tidak ditemukan\",\"English\":\"Account Number not found\"}}";
//		else if(mdlAPI.urlAPI.contentEquals("/passbooks/header"))
//			jsonResult = "{\"AccountNumber\":\"0080000677\",\"AccountType\":\"110\",\"AccountName\":\"NENGSIH FITRIA\",\"ShortBranchName\":\"KCU BANDUNG\",\"BranchName\":\"KCU BANDUNG\",\"PageNumber\":\"01\",\"CustomerDetail\":[{\"CustomerNumber\":\"00000006359\",\"CustomerName\":\"NENGSIH FITRIA\",\"RelationType\":\"PEMILIK\",\"AccountOwnership\":\"P\",\"CustomerAddress\":[\"KECAMATAN TANAH ABANG\",\"JL KEBON KACANG 34\",\"\"]}]}";
//		else if(mdlAPI.urlAPI.contentEquals("/passbooks/header-error"))
//			jsonResult = "{\"ErrorCode\":\"ESB-07-556\",\"ErrorMessage\": {\"Indonesian\":\"Transaksi tidak dapat diproses\",\"English\":\"Transaction cannot be processed\"}}";
//		else if(mdlAPI.urlAPI.contentEquals("/passbooks/regular-passbooks/account-statements"))
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"3,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"734,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"4,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"738,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"5,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"743,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"6,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"749,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"7,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"756,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"8,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"764,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"9,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"773,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"},{\"TransactionDate\":\"10/04/17\",\"TransactionAmount\":\"10,000.00\",\"TransactionCode\":\"0608\",\"RunningBalance\":\"783,000.00\",\"TransactionType\":\"Credit\",\"TransactionDescription\":\"KAS\",\"BranchCode\":\"00008\"}]}";
//		else if(mdlAPI.urlAPI.contentEquals("/passbooks/regular-passbooks/account-statements-error"))
//			jsonResult = "{\"ErrorCode\":\"ESB-18-281\",\"ErrorMessage\": {\"Indonesian\":\"Saldo buku yang Anda masukkan salah\",\"English\":\"The passbook balance is incorrect\"}}";
//		else if(mdlAPI.urlAPI.contentEquals("/passbooks/small-passbooks/account-statements"))
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"Transaction\":\"050417 KAS            4,000.00 K          610,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            5,000.00 K     615,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            6,000.00 K     621,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            7,000.00 K     628,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            8,000.00 K     636,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           10,000.00 K    646,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           11,000.00 K    657,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           12,000.00 K    669,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           13,000.00 K    682,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"}]}";
//			jsonResult = "{\"TransactionIndicator\":\"MP\",\"TransactionRecord\":[{\"Transaction\":\"050417 KAS            4,000.00 K     610,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            5,000.00 K     615,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            6,000.00 K     621,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            7,000.00 K     628,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS            8,000.00 K     636,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           10,000.00 K    646,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           11,000.00 K    657,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           12,000.00 K    669,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           13,000.00 K    682,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI                                     \"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"},{\"Transaction\":\"050417 KAS           14,000.00 K    696,000.00 0900\",\"Trailer\":\"------ SETORAN TUNAI\"}]}";
//		else if(mdlAPI.urlAPI.contentEquals("/passbooks/small-passbooks/account-statements-error"))
//			jsonResult = "{\"ErrorCode\":\"ESB-18-285\",\"ErrorMessage\": {\"Indonesian\":\"Nomor baris buku yang Anda masukkan salah\",\"English\":\"The passbook line number is incorrect\"}}";

//		if(mdlAPI.urlAPI.equals("/alert/mm"))
//			jsonResult = "{\"ErrorCode\":\"MOMO-103-1020\",\"ErrorMessage\":{\"Indonesian\": \"Ada request lain dari perangkat yang sama belum lama ini\",\"English\": \"There was another request from this device recently\"}}";
//			jsonResult = "{\"ErrorCode\": \"MOMO-103-1012\",\"ErrorMessage\": {\"English\": \"AlertType / AccountType / TransactionType not match\",\"Indonesian\": \"AlertType / AccountType / TransactionType tidak sesuai\"}}";

		return jsonResult;
	}

	//get client id
	@RequestMapping(value = "/get-clientid",method = RequestMethod.GET)
   	public @ResponseBody model.mdlResult GetClientID(@RequestParam(value="username", defaultValue="") String UserName,
   													@RequestParam(value="appname", defaultValue="") String AppName, HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlClientID mdlClientID = new model.mdlClientID();
		String ClientID;

		try {
			ClientID = ClientIdAdapter.getClientID(UserName, AppName);
			if (ClientID.equals("")){
				mdlResult.Title = "GetClientID";
				mdlResult.ErrorCode = "01";
				mdlResult.ErrorMessage = "Client ID gagal didapatkan, kemungkinan dikarenakan username dan appname Anda tidak terdaftar atau tidak aktif, silahkan hubungi admin server INVENT";
				mdlClientID.UserName = UserName;
				mdlClientID.AppName = AppName;
				mdlClientID.ClientID = "-";
				mdlResult.Result = mdlClientID;
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			}else {
				mdlResult.Title = "GetClientID";
				mdlResult.ErrorCode = "00";
				mdlResult.ErrorMessage = "Berhasil";
				mdlClientID.UserName = UserName;
				mdlClientID.AppName = AppName;
				mdlClientID.ClientID = ClientID;
				mdlResult.Result = mdlClientID;
				response.setStatus(HttpServletResponse.SC_OK);
			}

			logger.info("SUCCESS. API : get-clientid, method : GET, username:" + UserName + ", appname : " + AppName);
		}catch(Exception ex){
			mdlResult.Title = "GetClientID";
			mdlResult.ErrorCode = "99";
			mdlResult.ErrorMessage = "Sistem tidak tersedia";
			mdlClientID.UserName = UserName;
			mdlClientID.AppName = AppName;
			mdlClientID.ClientID = "-";
			mdlResult.Result = mdlClientID;
			logger.info("FAILED. API : get-clientid, method : GET, username:" + UserName + ", "
					+ "appname : " + AppName + ", Exception : " + ex.toString());
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		
		return mdlResult;
	}

	//get token
	@RequestMapping(value = "/token",method = RequestMethod.POST,consumes="application/x-www-form-urlencoded")
	public @ResponseBody model.mdlResult Token(@RequestHeader("Authorization") String Authorization,
            							@RequestHeader("Content-Type") String ContentType,
            							@RequestParam(value="grant_type", defaultValue="") String GrantType,
            							HttpServletResponse response)
	{
		String clientID = Authorization.replace("Basic ", "");
		String bodyGrantType = GrantType;
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlToken mdlToken = new model.mdlToken();
		
		try{
			if(bodyGrantType.equals("client_credentials")){
				String decryptedClientID = EncryptAdapter.decrypt(clientID, Globals.keyAPI);
				String[] decryptedClientIDArray = decryptedClientID.split("&&");
				String userName = decryptedClientIDArray[0];
				String appName = decryptedClientIDArray[1];
				
				Boolean checkUser = ClientIdAdapter.checkClientID(userName, appName);
				if (!checkUser){
					mdlResult.Title = "Token";
					mdlResult.ErrorCode = "01";
					mdlResult.ErrorMessage = "Client ID tidak valid, kemungkinan dikarenakan username dan appname Anda tidak terdaftar atau tidak aktif, silahkan hubungi admin server INVENT";
					mdlToken.grant_type = bodyGrantType;
					mdlToken.client_id = clientID;
					mdlResult.Result = mdlToken;
					response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				}else {
					Date now = new Date();
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					String startDate = df.format(now);
					String expiresIn = "3600"; //in second
					String token = clientID + "&&" + startDate + "&&" + expiresIn;
					String finalToken = EncryptAdapter.encrypt(token, Globals.keyAPI);
					
					mdlResult.Title = "Token";
					mdlResult.ErrorCode = "00";
					mdlResult.ErrorMessage = "Berhasil";
					mdlToken.grant_type = bodyGrantType;
					mdlToken.client_id = clientID;
					mdlToken.expires_in = expiresIn;
					mdlToken.start_date = startDate;
					mdlToken.access_token = finalToken;
					mdlResult.Result = mdlToken;
					response.setStatus(HttpServletResponse.SC_OK);
				}
			}
			else{
				mdlResult.Title = "Token";
				mdlResult.ErrorCode = "02";
				mdlResult.ErrorMessage = "Otentikasi grant type gagal";
				mdlToken.grant_type = bodyGrantType;
				mdlToken.client_id = clientID;
				mdlResult.Result = mdlToken;
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			}
			
			logger.info("SUCCESS. API : token, method: POST, accept: application/x-www-form-urlencoded, authorization: "+ Authorization
					 + ", grant type:" + bodyGrantType);
		}
		catch(Exception ex){
			mdlResult.Title = "Token";
			mdlResult.ErrorCode = "99";
			mdlResult.ErrorMessage = "Sistem tidak tersedia";
			mdlToken.grant_type = bodyGrantType;
			mdlToken.client_id = clientID;
			mdlResult.Result = mdlToken;
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			logger.error("FAILED. API : token, method: POST, accept: application/x-www-form-urlencoded, authorization: "+ Authorization
					 + ", grant type:" + bodyGrantType + "Exception:" + ex.toString());
		}
		
		return mdlResult;
	}

	//testing method get by get customer data
	@RequestMapping(value = "/get-customer-data",method = RequestMethod.GET)
   	public @ResponseBody model.mdlResult GetCustomerData(@RequestParam(value="customerid", defaultValue="") String CustomerID,
											   			@RequestHeader("Authorization") String Authorization, 
											   			HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlCustomer mdlCustomer = new model.mdlCustomer();
		model.mdlAuthentication mdlAuthentication = new model.mdlAuthentication();
		String token = Authorization.replace("Bearer ", "");
		
		try {			
			mdlAuthentication = TokenAdapter.GetAuthentication(token);
			String userName = mdlAuthentication.UserName;
			String appName = mdlAuthentication.AppName;
			String tokenStartDate = mdlAuthentication.TokenStartDate;
			String tokenExpiresIn = mdlAuthentication.TokenExpiresIn;
			
			Boolean checkUser = ClientIdAdapter.checkClientID(userName, appName);
			if (!checkUser){
				mdlResult.Title = "GetClientInfo";
				mdlResult.ErrorCode = "01";
				mdlResult.ErrorMessage = "Client ID tidak valid, kemungkinan dikarenakan username dan appname Anda tidak terdaftar atau tidak aktif, silahkan hubungi admin server INVENT";
				mdlResult.Result = mdlCustomer;
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			}
			else 
			{
				Boolean tokenIsActive = TokenAdapter.CheckToken(tokenStartDate, tokenExpiresIn, token);
				if(tokenIsActive){
					mdlCustomer = CustomerAdapter.GetCustomerData(CustomerID);
					if (mdlCustomer.CustomerID == null){
						mdlResult.Title = "GetCustomerData";
						mdlResult.ErrorCode = "03";
						mdlResult.ErrorMessage = "Informasi pelanggan tidak ditemukan, kemungkinan dikarenakan pelanggan tidak terdaftar.";
						mdlResult.Result = mdlCustomer;
						response.setStatus(HttpServletResponse.SC_OK);
					}else {
						mdlResult.Title = "GetCustomerData";
						mdlResult.ErrorCode = "00";
						mdlResult.ErrorMessage = "Berhasil";
						mdlResult.Result = mdlCustomer;
						response.setStatus(HttpServletResponse.SC_OK);
					}
				}
				else
				{
					mdlResult.Title = "GetCustomerData";
					mdlResult.ErrorCode = "02";
					mdlResult.ErrorMessage = "Token sudah tidak valid, silahkan melakukan request token kembali.";
					mdlResult.Result = mdlCustomer;
					response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				}
			}

			logger.info("SUCCESS. API : get-customer-data, method : GET, customerid:" + CustomerID);
		}catch(Exception ex){
			mdlCustomer = new model.mdlCustomer();
			
			mdlResult.Title = "GetCustomerData";
			mdlResult.ErrorCode = "99";
			mdlResult.ErrorMessage = "Sistem tidak tersedia";
			mdlResult.Result = mdlCustomer;
			logger.info("FAILED. API : get-customer-data, method : GET, customerid:" + CustomerID + ", "
					+ "Exception : " + ex.toString());
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		
		return mdlResult;
	}

	//testing method post by insert customer data
	@RequestMapping(value = "/insert-customer-data",method = RequestMethod.POST,consumes="application/json")
	public @ResponseBody model.mdlResult InsertCustomerData(@RequestHeader("Authorization") String Authorization,
            							@RequestHeader("Content-Type") String ContentType,
            							@RequestBody model.mdlCustomer mdlCustomerData,
            							HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlAuthentication mdlAuthentication = new model.mdlAuthentication();
		String token = Authorization.replace("Bearer ", "");
		
		try {			
			mdlAuthentication = TokenAdapter.GetAuthentication(token);
			String userName = mdlAuthentication.UserName;
			String appName = mdlAuthentication.AppName;
			String tokenStartDate = mdlAuthentication.TokenStartDate;
			String tokenExpiresIn = mdlAuthentication.TokenExpiresIn;
			
			Boolean checkUser = ClientIdAdapter.checkClientID(userName, appName);
			if (!checkUser){
				mdlResult.Title = "InsertCustomerData";
				mdlResult.ErrorCode = "01";
				mdlResult.ErrorMessage = "Client ID tidak valid, kemungkinan dikarenakan username dan appname Anda tidak terdaftar atau tidak aktif, silahkan hubungi admin server INVENT";
				mdlResult.Result = mdlCustomerData;
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			}
			else 
			{
				Boolean tokenIsActive = TokenAdapter.CheckToken(tokenStartDate, tokenExpiresIn, token);
				if(tokenIsActive){
					String result = CustomerAdapter.InsertCustomerData(mdlCustomerData);
					if(result.equals("Success Insert Customer")){
						mdlResult.Title = "InsertCustomerData";
						mdlResult.ErrorCode = "00";
						mdlResult.ErrorMessage = "Penambahan data pelanggan berhasil.";
						mdlResult.Result = mdlCustomerData;
						response.setStatus(HttpServletResponse.SC_OK);
					}
					else {
						mdlResult.Title = "InsertCustomerData";
						mdlResult.ErrorCode = "03";
						mdlResult.ErrorMessage = "Penambahan data pelanggan gagal.";
						mdlResult.Result = mdlCustomerData;
						response.setStatus(HttpServletResponse.SC_OK);
					}
				}
				else
				{
					mdlResult.Title = "InsertCustomerData";
					mdlResult.ErrorCode = "02";
					mdlResult.ErrorMessage = "Token sudah tidak valid, silahkan melakukan request token kembali.";
					mdlResult.Result = mdlCustomerData;
					response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				}
			}

			logger.info("SUCCESS. API : insert-customer-data, method : POST, jsonIn:" + mdlCustomerData);
		}catch(Exception ex){
			mdlResult.Title = "InsertCustomerData";
			mdlResult.ErrorCode = "99";
			mdlResult.ErrorMessage = "Sistem tidak tersedia";
			mdlResult.Result = mdlCustomerData;
			logger.info("FAILED. API : insert-customer-data, method : POST, jsonIn:" + mdlCustomerData + ", "
					+ "Exception : " + ex.toString());
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		
		return mdlResult;
	}
	
	//testing method put by update customer data
	@RequestMapping(value = "/update-customer-data",method = RequestMethod.PUT,consumes="application/json")
	public @ResponseBody model.mdlResult UpdateCustomerData(@RequestHeader("Authorization") String Authorization,
            							@RequestHeader("Content-Type") String ContentType,
            							@RequestParam(value="customerid", defaultValue="") String CustomerID,
            							@RequestBody model.mdlCustomer mdlCustomer,
            							HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlAuthentication mdlAuthentication = new model.mdlAuthentication();
		String token = Authorization.replace("Bearer ", "");
		
		try {			
			mdlAuthentication = TokenAdapter.GetAuthentication(token);
			String userName = mdlAuthentication.UserName;
			String appName = mdlAuthentication.AppName;
			String tokenStartDate = mdlAuthentication.TokenStartDate;
			String tokenExpiresIn = mdlAuthentication.TokenExpiresIn;
			
			Boolean checkUser = ClientIdAdapter.checkClientID(userName, appName);
			if (!checkUser){
				mdlResult.Title = "UpdateCustomerData";
				mdlResult.ErrorCode = "01";
				mdlResult.ErrorMessage = "Client ID tidak valid, kemungkinan dikarenakan username dan appname Anda tidak terdaftar atau tidak aktif, silahkan hubungi admin server INVENT";
				mdlResult.Result = mdlCustomer;
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			}
			else 
			{
				Boolean tokenIsActive = TokenAdapter.CheckToken(tokenStartDate, tokenExpiresIn, token);
				if(tokenIsActive){
					String result = CustomerAdapter.UpdateCustomerData(mdlCustomer);
					if(result.equals("Success Update Customer")){
						mdlResult.Title = "UpdateCustomerData";
						mdlResult.ErrorCode = "00";
						mdlResult.ErrorMessage = "Pengubahan data pelanggan berhasil.";
						mdlResult.Result = mdlCustomer;
						response.setStatus(HttpServletResponse.SC_OK);
					}
					else {
						mdlResult.Title = "UpdateCustomerData";
						mdlResult.ErrorCode = "03";
						mdlResult.ErrorMessage = "Pengubahan data pelanggan gagal.";
						mdlResult.Result = mdlCustomer;
						response.setStatus(HttpServletResponse.SC_OK);
					}
				}
				else
				{
					mdlResult.Title = "UpdateCustomerData";
					mdlResult.ErrorCode = "02";
					mdlResult.ErrorMessage = "Token sudah tidak valid, silahkan melakukan request token kembali.";
					mdlResult.Result = mdlCustomer;
					response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				}
			}

			logger.info("SUCCESS. API : update-customer-data, method : PUT, jsonIn:" + mdlCustomer);
		}catch(Exception ex){
			mdlResult.Title = "UpdateCustomerData";
			mdlResult.ErrorCode = "99";
			mdlResult.ErrorMessage = "Sistem tidak tersedia";
			mdlResult.Result = mdlCustomer;
			logger.info("FAILED. API : update-customer-data, method : PUT, jsonIn:" + mdlCustomer + ", "
					+ "Exception : " + ex.toString());
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		
		return mdlResult;
	}

	//testing method delete by delete customer data
	@RequestMapping(value = "/delete-customer-data",method = RequestMethod.DELETE)
	public @ResponseBody model.mdlResult DeleteCustomerData(@RequestHeader("Authorization") String Authorization,
            							@RequestParam(value="customerid", defaultValue="") String CustomerID,
            							HttpServletResponse response)
	{
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlAuthentication mdlAuthentication = new model.mdlAuthentication();
		String token = Authorization.replace("Bearer ", "");
		
		try {			
			mdlAuthentication = TokenAdapter.GetAuthentication(token);
			String userName = mdlAuthentication.UserName;
			String appName = mdlAuthentication.AppName;
			String tokenStartDate = mdlAuthentication.TokenStartDate;
			String tokenExpiresIn = mdlAuthentication.TokenExpiresIn;
			
			Boolean checkUser = ClientIdAdapter.checkClientID(userName, appName);
			if (!checkUser){
				mdlResult.Title = "UpdateCustomerData";
				mdlResult.ErrorCode = "01";
				mdlResult.ErrorMessage = "Client ID tidak valid, kemungkinan dikarenakan username dan appname Anda tidak terdaftar atau tidak aktif, silahkan hubungi admin server INVENT";
				mdlResult.Result = null;
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			}
			else 
			{
				Boolean tokenIsActive = TokenAdapter.CheckToken(tokenStartDate, tokenExpiresIn, token);
				if(tokenIsActive){
					String result = CustomerAdapter.DeleteCustomerData(CustomerID);
					if(result.equals("Success Delete Customer")){
						mdlResult.Title = "DeleteCustomerData";
						mdlResult.ErrorCode = "00";
						mdlResult.ErrorMessage = "Penghapusan data pelanggan berhasil.";
						mdlResult.Result = CustomerID;
						response.setStatus(HttpServletResponse.SC_OK);
					}
					else {
						mdlResult.Title = "DeleteCustomerData";
						mdlResult.ErrorCode = "03";
						mdlResult.ErrorMessage = "Penghapusan data pelanggan gagal.";
						mdlResult.Result = CustomerID;
						response.setStatus(HttpServletResponse.SC_OK);
					}
				}
				else
				{
					mdlResult.Title = "DeleteCustomerData";
					mdlResult.ErrorCode = "02";
					mdlResult.ErrorMessage = "Token sudah tidak valid, silahkan melakukan request token kembali.";
					mdlResult.Result = null;
					response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				}
			}

			logger.info("SUCCESS. API : delete-customer-data, method : DELETE, customerid:" + CustomerID);
		}catch(Exception ex){
			mdlResult.Title = "DeleteCustomerData";
			mdlResult.ErrorCode = "99";
			mdlResult.ErrorMessage = "Sistem tidak tersedia";
			mdlResult.Result = null;
			logger.info("FAILED. API : delete-customer-data, method : DELETE, customerid:" + CustomerID + ", "
					+ "Exception : " + ex.toString());
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		
		return mdlResult;
	}
}
